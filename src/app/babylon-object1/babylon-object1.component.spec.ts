import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BabylonObject1Component } from './babylon-object1.component';

describe('BabylonObject1Component', () => {
  let component: BabylonObject1Component;
  let fixture: ComponentFixture<BabylonObject1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BabylonObject1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BabylonObject1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
