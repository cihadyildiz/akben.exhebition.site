import * as BABYLON from 'babylonjs';


export class Game {

    private _canvas: HTMLCanvasElement;
    private _engine: BABYLON.Engine;
    private _scene: BABYLON.Scene;
    private _camera: BABYLON.TargetCamera;
    private _light: BABYLON.Light;
    constructor(canvasElement: string) {
        this._canvas = <HTMLCanvasElement>document.getElementById(canvasElement);
        this._engine = new BABYLON.Engine(this._canvas, true);
    }

    createScene(): void {
        // create a basic BJS Scene object
        this._scene = new BABYLON.Scene(this._engine);

        // create a FreeCamera, and set its position to (x:0, y:5, z:-10)
        this._camera = new BABYLON.TargetCamera('camera1', new BABYLON.Vector3(0, 5, -10), this._scene);

        // target the camera to scene origin
        this._camera.setTarget(BABYLON.Vector3.Zero());

        // attach the camera to the canvas
        this._camera.attachControl(this._canvas, false);

        // create a basic light, aiming 0,1,0 - meaning, to the sky
        this._light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), this._scene);


        // create a built-in "sphere" shape; with 16 segments and diameter of 2
        const sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, this._scene);
        //const sphere2 = BABYLON.Mesh.CreateSphere('sphere2', 20, 3, this._scene);
        sphere.rotation.x = Math.PI / 2;
        sphere.position.y = 1;
        sphere.actionManager = new BABYLON.ActionManager(this._scene);

        const hl1 = new BABYLON.HighlightLayer('hl1', this._scene);


        const ground = BABYLON.MeshBuilder.CreateGround('ground1',
            { width: 6, height: 6, subdivisions: 2 }, this._scene);

        const pointerDragBehavior = new BABYLON.PointerDragBehavior({ dragAxis: new BABYLON.Vector3(0, 0, 0) });
        pointerDragBehavior.useObjectOrienationForDragging = false;
        pointerDragBehavior.updateDragPlane = false;

        pointerDragBehavior.onDragStartObservable.add((event) => {
            console.log('dragStart');
            console.log(event);
           

        })
        pointerDragBehavior.onDragObservable.add((event) => {
            console.log('drag');
            console.log(event);
            sphere.position.y = event.dragPlanePoint.y;
            sphere.position.x = event.dragPlanePoint.x;
        })
        pointerDragBehavior.onDragEndObservable.add((event) => {
            console.log('dragEnd');
            console.log(event);

        })
        sphere.addBehavior(pointerDragBehavior);

        const h1 = new BABYLON.HighlightLayer('hl', this._scene);

        let lastClickedMesh = null;

        this._scene.onPointerObservable.add((evt) => {
            if (evt.pickInfo.hit && evt.pickInfo.pickedMesh !== undefined) {
                const mesh = evt.pickInfo.pickedMesh;
                if (mesh.name.startsWith('sphere')) {
                    if (lastClickedMesh !== null) {
                        console.log('unhighlighting', lastClickedMesh);
                        h1.removeMesh(lastClickedMesh);
                    }
                    lastClickedMesh = mesh;
                    h1.addMesh(lastClickedMesh, BABYLON.Color3.Green());
                }
            }
        }, BABYLON.PointerEventTypes.POINTERDOWN);

        // move the sphere upward 1/2 of its height


        // create a built-in "ground" shape

    }

    animate(): void {
        // run the render loop
        this._engine.runRenderLoop(() => {
            this._scene.render();
        });

        // the canvas/window resize event handler
        window.addEventListener('resize', () => {
            this._engine.resize();
        });
    }
}
